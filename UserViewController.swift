//
//  UserViewController.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-08-15.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit
import SDWebImage

class UserViewController: UIViewController {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBAction func connectWithFacebookButtonPress(_ sender: Any) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomObservers()
        updateData()
        // Do any additional setup after loading the view.
    }
    
    func addCustomObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(UserViewController.updateData), name: Notification.Name(rawValue: "didUpdateCurrentUser"), object: nil)
    }

    func updateData() {
        if let user = User.user {
            print(user.canonicalUserName)
            userNameLabel.text = user.canonicalUserName
            if let sptImage = user.largestImage {
                userImage.sd_setIndicatorStyle(.gray)
                userImage.sd_setImage(with: sptImage.imageURL, completed: nil)
            } else {
                userImage.image = UIImage.init(named: "no-image.png")
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
