//
//  SearchSpotifyController.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-07-01.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class SearchSpotifyController: UIViewController, UITableViewDelegate, UITableViewDataSource,
UISearchBarDelegate {
    
    //MARK: Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: Actions
    @IBAction func backAction(_ sender: Any) {
        performSegue(withIdentifier: "unwindToPartiDetail", sender: self)
    }
    
    
    @IBOutlet var tableView: UITableView!
    var partialTracks: [SPTPartialTrack] = []
    var playerViewController = PlayerViewController()
    var songToAdd:SPTPartialTrack!
    var receivedParti:Parti!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchSong(songName: String) {
        getPartialTracksForQuery(query: songName) { (tracks) in
            self.partialTracks = tracks
            self.reloadTable()
        }
    }
    
    func reloadTable() {
        if (self.tableView != nil) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBarSearch(input: searchBar.text!)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBarSearch(input: searchBar.text!)
        searchBar.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.partialTracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedTrackCell", for: indexPath) as! DetailedTrackViewCell
        if self.partialTracks.count < indexPath.row {
            return cell
        }
        cell.trackNameLabel.text = self.partialTracks[indexPath.row].name
        cell.artistNameLabel.text = self.partialTracks[indexPath.row].album.name
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < self.partialTracks.count {
            songToAdd = partialTracks[indexPath.row]
            performSegue(withIdentifier: "unwindToPartiDetail", sender: self)
        }
    }
    
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (songToAdd != nil) {
            // get a reference to the second view controller
            let partiDetailVC = segue.destination as! PartiDetailViewController
            
            // set a variable in the second view controller with the data to pass
            partiDetailVC.receivedSong = songToAdd
            partiDetailVC.receivedParti = self.receivedParti
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    
    // HELP METHODS
    
    func searchBarSearch(input: String){
        self.searchSong(songName: input)
    }
}
