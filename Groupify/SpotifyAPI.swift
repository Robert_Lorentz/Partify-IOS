//
//  SpotifyAPI.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-08-07.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation



func getTrackInfo(uri: String, callback: @escaping (SPTTrack) -> ()) {
    if (!isLoggedIn()) {
        return
    }
    let uriURL = URL.init(string: uri)
    SPTTrack.track(withURI: uriURL, accessToken: PlayerInformation.session.accessToken, market: nil) { (error, track) in
        if (error != nil) {
            print(error!)
        } else {
            if let track = track as? SPTTrack {
                callback(track)
            } else {
                print("error parsing track")
            }
        }
    }
}

func getTracksInfo(uris: [String], callback: @escaping ([SPTTrack]) -> ()) {
    if (!isLoggedIn()) {
        return
    }
    let uriURLS = uris.flatMap(URL.init(string:))
    SPTTrack.tracks(withURIs: uriURLS, accessToken: PlayerInformation.session.accessToken, market: nil) { (error, tracks) in
        if (error != nil) {
            print(error!)
        } else {
            if let tracks = tracks as? [SPTTrack] {
                callback(tracks)
            } else {
                print("error parsing tracks")
            }
        }
    }
}

//Search for a song with a query, returns a list of SPTPartialTracks
func getPartialTracksForQuery(query: String, callback_outer: @escaping ([SPTPartialTrack]) -> ()) {
    SPTSearch.perform(withQuery: query, queryType: SPTSearchQueryType.queryTypeTrack, accessToken: PlayerInformation.session.accessToken, callback: { (error, result) in
        if (error != nil) {
            print(error!)
        } else {
            if let pageResult = result as? SPTListPage {
                if let tracks = pageResult.items as? [SPTPartialTrack] {
                    callback_outer(tracks)
                }
            }
        }
    })
}

func isLoggedIn() -> Bool {
    if let player = PlayerInformation.player {
        return player.loggedIn
    } else {
        return false
    }
}

func getUser() {
    
}

func getCurrentUser(callback: @escaping (SPTUser) -> ()) {
    SPTUser.requestCurrentUser(withAccessToken: PlayerInformation.session.accessToken) { (error, user) in
        if error != nil {
            print(error!)
            return
        }
        callback(user as! SPTUser)
    }
}
