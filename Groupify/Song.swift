//
//  Song.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-06-29.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation

struct SimpleSong {
    let name : String
    let spotifyURI : String
}
