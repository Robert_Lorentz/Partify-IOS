//
//  CreatePartiTableViewController.swift
//  Partify
//
//  Created by Christian Ekenstedt on 2017-08-15.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CreatePartiTableViewController: UITableViewController {
    
    var setting:String?

    @IBOutlet weak var partiNameTextField: UITextField!
    @IBOutlet weak var pickedSetting: UILabel!
    
    // References
    let root : DatabaseReference! = Database.database().reference()
    let rParties = Database.database().reference().child("Parties")

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        addCustomObservers()
        initialize()
    }
    
    func initialize(){
        if let _ = setting{
            
        }
    }
    
    func createParti(){
        // Check all data
        if let partiName = partiNameTextField.text, let partiPrivacySetting = setting{
            if partiName.characters.count > 3 && partiPrivacySetting.characters.count > 0 {
                
                print("Sending Firebase request to create parti -> \(partiName) with setting -> \(partiPrivacySetting)")
                
                var settingsDict = [String:String]() // Set all settings in a dict.
                settingsDict["Privacy"] = partiPrivacySetting
                FirebaseAPI.sharedInstance.writeNewParti(name: partiName, settings: settingsDict)
            }else{
                print("Name length is to short.")
            }
        }else{
            print("Everything is not set. Check data.")
        }
    }
    
    @IBAction func unwindToCreatePartiDetail(segue: UIStoryboardSegue) {
        
        pickedSetting.text = setting
        
    }
    
    func addCustomObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(CreatePartiTableViewController.createParti), name: NSNotification.Name.getNewPartiDetails , object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
/*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
*/
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation
*/
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "pickPrivacySetting" {
            let dest = segue.destination as! PrivacyPickerViewController
            dest.selectedPrivacySetting = self.setting
        }
        print(partiNameTextField.text!)
        print(segue.destination)
        
    }
    

}
