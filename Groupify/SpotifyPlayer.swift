//
//  SpotifyPlayer.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-07-17.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//
import Foundation

struct PlayerInformation {
    static var player: SPTAudioStreamingController?
    static var session: SPTSession!
}


class SpotifyPlayer: NSObject, SPTAudioStreamingPlaybackDelegate, SPTAudioStreamingDelegate {

    //Singleton class
    static let sharedInstance = SpotifyPlayer()
    
    var auth = SPTAuth.defaultInstance()!
    var loginUrl: URL?
    
    var currentPlaylist: [SPTTrack] = []
    var currentlyPlaying: SPTTrack?
    var indexOfSong: Int?
    var playingParti: Parti?
    var thisDeviceIsPlaying: Bool?
    var thisPartiIsPlaying: Bool?
    
    private override init() {
        super.init()
        print("Spotify player initiated")
        PlayerInformation.player?.playbackDelegate = self
        PlayerInformation.player?.delegate = self
        
    }
    
    func initializePlayer(authSession:SPTSession){
        if PlayerInformation.player == nil {
            PlayerInformation.player = SPTAudioStreamingController.sharedInstance()
            try! PlayerInformation.player!.start(withClientId: auth.clientID)
            PlayerInformation.player!.login(withAccessToken: authSession.accessToken)
        }
    }
    
    
    func openLogin (){
        
        // Your webView code goes here
        let appUrl = loginUrl!
        let webUrl = auth.spotifyWebAuthenticationURL()!
        let url = UIApplication.shared.canOpenURL(appUrl) ? appUrl : webUrl
        if UIApplication.shared.canOpenURL(url) {
            //If you want handle the completion block than
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                    // print("Open url : \(success)")
                })
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func initiatePlayer(queuelist: [SPTTrack], currentlyPlaying: SPTTrack) {
        currentPlaylist = queuelist
        self.currentlyPlaying = currentlyPlaying
    }
    
    
    func setup(){
        SPTAuth.defaultInstance().clientID = "92bc567adadc47ec8d35210d726481f0"
        SPTAuth.defaultInstance().redirectURL = URL(string: "IOSPartify://")
        //SPTAuth.defaultInstance().tokenSwapURL =
        //SPTAuth.defaultInstance().tokenRefreshURL = SPTAuth.
        
        
        SPTAuth.defaultInstance().requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope, SPTAuthPlaylistModifyPublicScope, SPTAuthPlaylistModifyPrivateScope]
        // loginUrl = SPTAuth.defaultInstance().spotifyAppAuthenticationURL()
        
        loginUrl = SPTAuth.defaultInstance().spotifyWebAuthenticationURL()
    }
    
    
    func initiatePartiPlayer(parti: Parti, deviceIsPlaying: Bool, playlist: [SPTTrack]) {
        playingParti = parti
        currentPlaylist = playlist
        self.thisDeviceIsPlaying = deviceIsPlaying
        FirebaseAPI.sharedInstance.observeCurrentlyPlayingParti(parti: parti, callback: { (index, isPlaying) in
            if let index = index {
                self.indexOfSong = index
                self.thisPartiIsPlaying = isPlaying
                if !isPlaying {
                    self.pause()
                } else {
                    if self.indexOfSong! < self.currentPlaylist.count {
                        self.playSong(track: self.currentPlaylist[self.indexOfSong!])
                    }
                }
                NotificationCenter.default.post(name: Notification.Name(rawValue: "didChangeDatabaseData"), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "didChangeMetadata"), object: nil)
            }
        })
    }
    
    func startPartiPlayer(toPlay: SPTTrack) {
        
        indexOfSong = currentPlaylist.index(of: toPlay)
        
        //stop currently playing parti
        if let lastParti = playingParti {
            FirebaseAPI.sharedInstance.writeStopPlayingToParti(parti: lastParti)
            FirebaseAPI.sharedInstance.removeAllObservers()
        }
        
        FirebaseAPI.sharedInstance.writeCurrentlyPlayingToParti(parti: playingParti!, playIndex: indexOfSong!, isPlaying: true)
        
        if let playing = thisDeviceIsPlaying, playing {
            playSong(track: toPlay)
        }
    }
    
    
    func isPlaying() -> Bool{
        if let isPlaying = PlayerInformation.player?.playbackState.isPlaying {
            return isPlaying
        }
        return false
    }
    
    func changePlaylist(playlist: [SPTTrack]) {
        currentPlaylist = playlist
    }
    
    func switchDeviceIsPlaying(isPlaying: Bool, parti: Parti) {
        if parti.id == playingParti?.id {
            thisDeviceIsPlaying = isPlaying
        }
    }
    
    func flipPlayStatus() {
        if let state = PlayerInformation.player?.playbackState, let parti = playingParti, let index = indexOfSong {
            
            let isPlaying = state.isPlaying
            PlayerInformation.player!.setIsPlaying(isPlaying ? false : true, callback: nil)
            FirebaseAPI.sharedInstance.writeCurrentlyPlayingToParti(parti: parti, playIndex: index, isPlaying: !isPlaying)
        }
    }
    
    func pause() {
        if let player = PlayerInformation.player {
            player.setIsPlaying(false, callback: nil)
        }
    }
    
    func resume() {
        if let player = PlayerInformation.player {
            player.setIsPlaying(true, callback: nil)
        }
    }
    
    func nextSong() {
        if let index = indexOfSong {
            let nextSongIndex = index+1
            if nextSongIndex < currentPlaylist.count {
                indexOfSong = nextSongIndex
                playSong(track: currentPlaylist[nextSongIndex])
            }
        }
    }
    
    func previousSong() {
        if let index = indexOfSong {
            let previousSongIndex = index-1
            if previousSongIndex > 0 && previousSongIndex < currentPlaylist.count {
                indexOfSong = previousSongIndex
                playSong(track: currentPlaylist[previousSongIndex])
            }
        }
    }
    func playSong(track: SPTTrack) {
        if let parti = playingParti, let index = indexOfSong {
            FirebaseAPI.sharedInstance.writeCurrentlyPlayingToParti(parti: parti, playIndex: index, isPlaying: true)
        }
        if let metadata = PlayerInformation.player?.metadata, let currentTrack = metadata.currentTrack {
            if currentTrack.uri == track.uri.absoluteString {
                resume()
                return
            }
        }
        let songURI = track.uri.absoluteString
        if let shouldPlay = thisDeviceIsPlaying, shouldPlay {
            PlayerInformation.player?.playSpotifyURI(songURI, startingWith: 0, startingWithPosition: 0, callback: { (error) in
                if (error != nil) {
                    print(error!)
                }
            })
        }
        currentlyPlaying = track
    }

    //MARK: Audio Streaming protocol methods
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChange metadata: SPTPlaybackMetadata!) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "didChangeMetadata"), object: nil)
    }
    
    func audioStreamingDidSkip(toNextTrack audioStreaming: SPTAudioStreamingController!) {
        //print("next track")
    }
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didStopPlayingTrack trackUri: String!) {
        nextSong()
    }
    
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        // after a user authenticates a session, the SPTAudioStreamingController is then initialized and this method called
        print("logged in")
    }
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didStartPlayingTrack trackUri: String!) {
        
    }
    
    func audioStreamingDidLosePermission(forPlayback audioStreaming: SPTAudioStreamingController!) {
        print("Lost permission")
    }
    
    func audioStreamingDidBecomeInactivePlaybackDevice(_ audioStreaming: SPTAudioStreamingController!) {
        print("became inactive playback device")
    }
    
    func audioStreamingDidEncounterTemporaryConnectionError(_ audioStreaming: SPTAudioStreamingController!) {
        print("temporary connection error")
    }
}
