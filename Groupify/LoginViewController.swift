//
//  LoginViewController.swift
//  Groupify
//
//  Created by Robert Lorentz on 18/06/17.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // OUTLETS
    @IBOutlet weak var connectButton: SPTConnectButton!
    
    // MARK - actions
    @IBAction func connectButtonPressed(_ sender: Any) {
        
        SpotifyPlayer.sharedInstance.openLogin()
    }
    
    // MEHTODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SpotifyPlayer.sharedInstance.setup()
        
        
        addCustomObservers()
        
        
        
        
    }
    
    
    /*
     if SPTAuth.defaultInstance().session.isValid() {
     let tc = self.storyboard!.instantiateViewController(withIdentifier: "TabBarController")
     self.present(tc, animated: true, completion: nil)
     self.show(tc, sender: self)
     }else{
     
     }
     */
    
    // Observers
    func addCustomObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.updateAfterFirstLogin), name: Notification.Name(rawValue: "loginSuccessfull"), object: nil)
    }
    
    // Help methods
    func updateAfterFirstLogin () {
        
        if let sessionObj:AnyObject = UserDefaults.standard.object(forKey: "SpotifySession") as AnyObject? {
            let sessionDataObj = sessionObj as! Data
            let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
            
            PlayerInformation.session = firstTimeSession
            
            SpotifyPlayer.sharedInstance.initializePlayer(authSession: PlayerInformation.session)
            
        }
        let tc = self.storyboard!.instantiateViewController(withIdentifier: "TabBarController")
        self.present(tc, animated: true, completion: nil)
        self.show(tc, sender: self)
    }
    
    
    //
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        if let sessionObj:AnyObject = UserDefaults.standard.object(forKey: "SpotifySession") as AnyObject? {
            let sessionDataObj = sessionObj as! Data
            let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
            
            PlayerInformation.session = firstTimeSession
            
        }
        
        let session = PlayerInformation.session
        
        if session == nil {
            print("session is nil")
        }else if  (session?.isValid())!{
            SpotifyPlayer.sharedInstance.initializePlayer(authSession: PlayerInformation.session)
            let tc = self.storyboard!.instantiateViewController(withIdentifier: "TabBarController")
            self.present(tc, animated: true, completion: nil)
            self.show(tc, sender: self)
        }else{
            // Session expired - we need to refresh it before continuing.
            // This process doesn't involve user interaction unless it fails.
            
        }
    }
    
}

