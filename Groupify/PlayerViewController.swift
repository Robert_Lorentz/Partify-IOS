//
//  playerController.swift
//  Groupify
//
//  Created by Robert Lorentz on 18/06/17.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    
    //MARK: Actions
    @IBAction func playButtonPress(_ sender: Any) {
        checkPlayPauseBtn()
        SpotifyPlayer.sharedInstance.flipPlayStatus()
    }
    @IBAction func nextButtonPress(_ sender: Any) {
        SpotifyPlayer.sharedInstance.nextSong()
    }
    @IBAction func previousButtonPress(_ sender: Any) {
        SpotifyPlayer.sharedInstance.previousSong()
    }
    
    // Initial
    override func viewDidLoad() {
        super.viewDidLoad()
        updateMetadata()
        addCustomObservers()
    }
    
    func checkPlayPauseBtn() {
        
        SpotifyPlayer.sharedInstance.isPlaying() ? playButton.setImage(#imageLiteral(resourceName: "play-button-2"), for: UIControlState.normal) : playButton.setImage(#imageLiteral(resourceName: "pause-button"), for: UIControlState.normal)
        
    }
    
    func addCustomObservers() {
        //Change metadata from shared player.
        NotificationCenter.default.addObserver(self, selector: #selector(PlayerViewController.updateMetadata), name: Notification.Name(rawValue: "didChangeMetadata"), object: nil)
    }

    
    func updateMetadata(){

        if SPTAudioStreamingController.sharedInstance().metadata == nil || SPTAudioStreamingController.sharedInstance().metadata.currentTrack == nil {
            self.albumCover.image = nil
            return
        }

        self.songName.text = SPTAudioStreamingController.sharedInstance().metadata.currentTrack?.name
        self.artistName.text = SPTAudioStreamingController.sharedInstance().metadata.currentTrack?.artistName
        self.albumName.text = SPTAudioStreamingController.sharedInstance().metadata.currentTrack?.albumName
        
        let track = SPTAudioStreamingController.sharedInstance().metadata.currentTrack
        let imageURL = track?.albumCoverArtURL
        if imageURL == nil {
            print("Album \(String(describing: track?.albumName)) doesn't have any images!")
            self.albumCover.image = nil
            return
        }
        // Pop over to a background queue to load the image over the network.
        
        DispatchQueue.global().async {
            do {
                let imageData = try Data(contentsOf: URL(string: imageURL!)!, options: [])
                let image = UIImage(data: imageData)
                // …and back to the main queue to display the image.
                DispatchQueue.main.async {
                    self.albumCover.image = image
                    if image == nil {
                        print("Couldn't load cover image with error.)")
                        return
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}

