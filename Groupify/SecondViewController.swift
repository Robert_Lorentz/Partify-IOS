//
//  SecondViewController.swift
//  Groupify
//
//  Created by Robert Lorentz on 18/06/17.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, , UITableViewDelegate, UITableViewDataSource {

    // MARK: - Properties
    var songs = [Song]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        songs = [
            Song(name: "Levels", artistName: "Avicii", spotifyURI: "spotify:track:0JhKJg5ejeQ8jq89UQtnw8"),
            Song(name: "Fade Into Darkness", artistName: "Avicii", spotifyURI: "spotify:track:6q1aghK6XbH6aNu6ryIbFI")
        ]
    }

    // MARK: - Table View
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let song = songs[indexPath.row]
        cell.detailTextLabel?.text = song.name
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
}

