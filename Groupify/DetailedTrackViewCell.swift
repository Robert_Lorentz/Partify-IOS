//
//  DetailedTrackViewCell.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-08-08.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit
import SDWebImage

class DetailedTrackViewCell: UITableViewCell {
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var speakerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadImageWithURL(imageURL: URL) {
        self.trackImage.sd_setShowActivityIndicatorView(true)
        self.trackImage.sd_setIndicatorStyle(.gray)
        self.trackImage.sd_setImage(with: imageURL) { (_, error, _, _) in
            if error != nil {
                print(error!)
            }
        }
    }
    
    func setNormalCell() {
        self.trackNameLabel.textColor = UIColor.black
        self.speakerImage.image = nil
    }
    
    func setPlayingCell() {
        self.trackNameLabel.textColor = UIColor.black
        self.speakerImage.image = UIImage.init(named: "speaker.png")
    }
    
    func setCell(name:String, albumName: String, imageURL: URL, isPlaying: Bool) {
        self.trackNameLabel.text = name
        self.artistNameLabel.text = albumName
        loadImageWithURL(imageURL: imageURL)
        isPlaying ? setPlayingCell() : setNormalCell()
    }
}
