//
//  UserPartiesController.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-07-16.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation
import FirebaseDatabase

class UserPartiesController: UIViewController, UITableViewDelegate, UITableViewDataSource,
UISearchBarDelegate {
    
    
    //MARK: Outlets
    @IBOutlet
    var tableView: UITableView!
    @IBOutlet weak var mySavedPartiesTableHeightConstraint: NSLayoutConstraint!
    //MARK: Actions
    
    @IBAction func unwindToUserParties(segue: UIStoryboardSegue) {
        
    }
    
    var parties = [Parti]()
    var playerViewController = PlayerViewController()
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiateTable()
        ref = Database.database().reference()
        observeParties()
    }
    
    
    func observeParties() {
        /*self.ref.child("Parties").observe(DataEventType.value, with: {(snapshot) in
            let partiDict = snapshot.value! as? [AnyObject]
            self.parties = []
            for parti in partiDict! {
                let partiName = parti["Name"] as! String
                let id = parti["id"] as! Int
                self.parties.append(Parti(name: partiName, id: id))
                self.adjustTableHeight()
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })*/
        
        self.ref.child("Parties").observe(.value, with: { (snapshot) in
            if snapshot.exists(){
                let partiDict = snapshot.value! as! [String:AnyObject]
                self.parties = []
                for p in partiDict  {
                    
                    let partiName = p.value["Name"] as! String
                    let id = p.value["id"] as! String
                    
                    self.parties.append(Parti(name: partiName, id: id))
                    self.adjustTableHeight()
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        })
    }
    
    func initiateTable() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.borderWidth = 0.5;
        tableView.layer.borderColor = UIColor.gray.cgColor
        
    }
    
    //Adjusts the 'my Saved Parties' table based on row height and amount of saved parties
    func adjustTableHeight() {
        mySavedPartiesTableHeightConstraint.constant = CGFloat.init(parties.count*45)
    }
    
    //MARK: TableView functions
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parties.count;    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PartiViewCell", for: indexPath) as! PartiViewCell
        if (indexPath.row > parties.count) {
            return cell
        }
        if (cell.partiNameLabel != nil) {
            cell.partiNameLabel.text = parties[indexPath.row].name
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.ref.removeAllObservers()
        if indexPath.row < self.parties.count {
            self.performSegue(withIdentifier: "partiSegue", sender: self.parties[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            //TODO: Delete the row at indexPath here
            self.deleteParti(row: indexPath.row)
            tableView.reloadData()
        }
        deleteAction.backgroundColor = .red
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            //TODO: edit the row at indexPath here
            self.editParti(row: indexPath.row)
            tableView.reloadData()
        }
        editAction.backgroundColor = .gray
        
        
        return [deleteAction, editAction]
        
    }
    
    func deleteParti(row: Int){
        let parti = parties[row]
        print("Parti - \(parti) to be deleted.")
    }
    
    func editParti(row: Int){
        let parti = parties[row]
        print("Parti - \(parti) to be edited.")
        
    }
    
    
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "partiSegue" {
            // get a reference to the second view controller
            let partiDetailVC = segue.destination as! PartiDetailViewController
            
            // set a variable in the second view controller with the data to pass
            partiDetailVC.receivedParti = sender as? Parti
        }
        else if segue.identifier == "createParti" {
            
        }
        

    }
}
