//
//  Track.swift
//  Partify
//
//  Created by Christian Ekenstedt on 2017-08-16.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation

struct Track {
    let key : String
    let spttrack : SPTTrack
}

func Playlist(tracks: [Track]) -> [SPTTrack] {
    return tracks.map({ t -> SPTTrack in
        t.spttrack
    })
}
