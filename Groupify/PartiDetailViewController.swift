//
//  PartiDetailViewController.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-07-17.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SDWebImage

class PartiDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK: Outlets & Properties
    
    @IBOutlet weak var voteOnlyModeSwitch: UISwitch!
    @IBOutlet weak var navigationBar: UINavigationBar!
    var receivedParti: Parti?
    @IBOutlet var tableView: UITableView!
    
    var playerViewController = PlayerViewController()
    var receivedSong: SPTPartialTrack!
    var playlist: [SPTTrack] = []
    var tracklist: [Track] = []
    let firebase = FirebaseAPI.sharedInstance
    var songsArray: [Dictionary<String, String>] = []
    

    //MARK: Actions
    
    @IBAction func voteOnlyModeAction(_ sender: Any) {
        if let parti = receivedParti {
            SpotifyPlayer.sharedInstance.switchDeviceIsPlaying(isPlaying: !voteOnlyModeSwitch.isOn, parti: parti)
        }
    }
    @IBAction func addSongButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "sendParti", sender: self)
    }

    @IBAction func backAction(_ sender: Any) {
        performSegue(withIdentifier: "unwindToUserParties", sender: self)
    }
    
    @IBAction func unwindToPartiDetail(segue: UIStoryboardSegue) {
        // add a song if one is added from search
        if let songToAdd = self.receivedSong {
            getTrackInfo(uri: songToAdd.playableUri.absoluteString, callback: { (track) in
                //self.playlist.append(Track)
                self.firebase.writeTrackToParti(parti: self.receivedParti!, newSong: track)
            })
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        tableView.delegate      =   self
        tableView.dataSource    =   self
    }
    
    func initialize() {
        addCustomObservers()
        if let rp = receivedParti {
            if let _ = self.navigationBar.topItem?.title {
                self.navigationBar.topItem!.title = rp.name
            }
            self.observeParti(parti: rp)
        }
        if (tableView != nil) {
            self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            self.tableView.reloadData()
        }
    }
    
    func reloadTable() -> (){
        if (self.tableView != nil) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func addCustomObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(PartiDetailViewController.reloadTable), name: Notification.Name(rawValue: "didStartPlayingTrack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PartiDetailViewController.reloadTable), name: Notification.Name(rawValue: "didChangeDatabaseData"), object: nil)

        SpotifyPlayer.sharedInstance.initiatePartiPlayer(parti: receivedParti!, deviceIsPlaying: !voteOnlyModeSwitch.isOn, playlist: Playlist(tracks: tracklist))
    }
    
    func observeParti(parti: Parti) {
        firebase.observePartiSonglist(parti: parti) { (tracks) in
            self.tracklist = tracks
            SpotifyPlayer.sharedInstance.changePlaylist(playlist: Playlist(tracks: tracks))
            self.reloadTable()
        }
    }
    
    //MARK: Table functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tracklist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedTrackCell", for: indexPath) as! DetailedTrackViewCell
        var isPlaying: Bool = false
        if let songIndex = SpotifyPlayer.sharedInstance.indexOfSong, let parti = SpotifyPlayer.sharedInstance.playingParti, let receivedParti = receivedParti {
            if songIndex == indexPath.row && parti.id == receivedParti.id {
                isPlaying = true
            }
        }
        let name = self.tracklist[indexPath.row].spttrack.name ?? "No track name"
        let albumName = self.tracklist[indexPath.row].spttrack.album.name ?? "No album name"
        let url = self.tracklist[indexPath.row].spttrack.album.largestCover.imageURL ??
            URL.init(fileURLWithPath: "no-image-found")
        cell.setCell(name: name, albumName: albumName, imageURL: url
            , isPlaying: isPlaying)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < self.tracklist.count {
            let trackToPlay = self.tracklist[indexPath.row].spttrack
            SpotifyPlayer.sharedInstance.startPartiPlayer(toPlay: trackToPlay)
        }
    }
    
    
    // Adds when swipe left on a row, "Delete"
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    // When pressing any editing button - this will be triggerd.
    // TODO: Implement delete row in Firebase on delete.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let track = tracklist[indexPath.row]
            deleteSongFromPlaylist(track: track)
            tableView.reloadData()
        }
    }
    
    func deleteSongFromPlaylist(track: Track){
        firebase.removeTrackFromPartiSonglist(parti: self.receivedParti!,track: track)
    }
    
    //MARK: Segue
    
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // get a reference to the second view controller
        if (segue.identifier == "sendParti") {
            let searchSpotifyVC = segue.destination as! SearchSpotifyController
            
            // set a variable in the second view controller with the data to pass
            searchSpotifyVC.receivedParti = self.receivedParti!
        }
    }
}
