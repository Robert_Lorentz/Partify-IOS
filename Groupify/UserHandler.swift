//
//  UserHandler.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-08-15.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation

struct User {
    static var user: SPTUser!
    static var savedParties: [Parti] = []
}

class UserHandler {
    
    static let sharedInstance = UserHandler()
    var firebase: FirebaseAPI
    
    private init() {
        firebase = FirebaseAPI.sharedInstance
        print("UserHandler initiated")
    }
    
    func initalize() {
        getCurrentUser { (user) in
            User.user = user
            NotificationCenter.default.post(name: Notification.Name(rawValue: "didUpdateCurrentUser"), object: nil)
            self.handleNewUser(outerCompletion: {() in
                self.firebase.observeUserSavedParties(user: User.user, callback: { (parties) in
                    User.savedParties = parties
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "didChangeUserParties"), object: nil)
                })
            })
        }
    }
    
    func handleNewUser(outerCompletion: @escaping () -> Void) {
        firebase.checkIfUserExists(user: User.user) { (exists) in
            if !exists {
                self.firebase.writeNewUser(user: User.user, completion: {() in
                    outerCompletion()
                })
            } else {
                outerCompletion()
            }
        }
    }
    
}
