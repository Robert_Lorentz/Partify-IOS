//
//  PartiViewCell.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-08-08.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class PartiViewCell: UITableViewCell {
    @IBOutlet weak var partiNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
