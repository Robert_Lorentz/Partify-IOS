//
//  CreatePartiViewController.swift
//  Partify
//
//  Created by Christian Ekenstedt on 2017-08-15.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class CreatePartiViewController: UIViewController {

    @IBOutlet weak var partitialView: UIView!
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.getNewPartiDetails, object: nil)
        performSegue(withIdentifier: "unwindToUserParties", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
