//
//  DetailedPlayerViewController.swift
//  Partify
//
//  Created by Christian Ekenstedt on 2017-08-17.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import UIKit

class DetailedPlayerViewController: UIViewController , UIViewControllerTransitioningDelegate {

    // Outlets
    @IBOutlet weak var partiName: UILabel!
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var bgCover: UIImageView!
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var rewindButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func playButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func forwardButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func rewindButtonPressed(_ sender: Any) {
        
    }
    
    // Delegate
    
    
    // Other
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
