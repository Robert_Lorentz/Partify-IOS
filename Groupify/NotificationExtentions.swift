//
//  NotificationExtentions.swift
//  Partify
//
//  Created by Christian Ekenstedt on 2017-08-04.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

extension Notification.Name {
    static let myNotification = Notification.Name("playSong")
    
    static let getNewPartiDetails = Notification.Name("getNewPartiDetails")
    static let sendNewPartiDetails = Notification.Name("sendNewPartiDetails")
}
