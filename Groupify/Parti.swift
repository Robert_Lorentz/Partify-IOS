//
//  Parti.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-07-16.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation

struct Parti {
    let name: String
    let id: String
}
