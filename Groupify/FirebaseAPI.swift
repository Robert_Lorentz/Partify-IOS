//
//  FirebaseAPI.swift
//  Partify
//
//  Created by Robert Lorentz on 2017-08-07.
//  Copyright © 2017 Robert Lorentz. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FirebaseAPI {
    
    // References
    static let sharedInstance = FirebaseAPI()
    var ref: DatabaseReference!
    let root : DatabaseReference! = Database.database().reference()
    let rParties = Database.database().reference().child("Parties")
    let rUsers = Database.database().reference().child("Users")
    
    private init() {
        ref = Database.database().reference()
    }

    //Observes the songlist of the given parti and runs the callback on the tracks
    func observePartiSonglist(parti: Parti, callback: @escaping ([Track]) -> ()) {
        self.ref.child("Parties/\(parti.id)/Songlist").observe(DataEventType.value, with: {(snapshot) in
            if snapshot.exists() {
                var uris: [String] = []
                var keys: [String] = []
                for s in snapshot.value as! [String:AnyObject] {
                    keys.append(s.key)
                    if let uri = s.value["URI"] as? String {
                        uris.append(uri)
                    }
                }

                getTracksInfo(uris: uris, callback: { (tracks) in
                    callback(zip(tracks, keys).map({ track, key -> Track in
                        return Track.init(key: key, spttrack: track)
                    }))
                })
            }
        })
    }
    
    func removeTrackFromPartiSonglist(parti: Parti, track: Track) {
        self.ref.child("Parties/\(parti.id)/Songlist/\(track.key)").removeValue()
    }

    //Writes a track to the songlist in firebase
    func writeTrackToParti(parti: Parti, newSong: SPTTrack) {
        let s = NSMutableDictionary()
        s.setValue(newSong.uri.absoluteString, forKey: "URI")
        
        let songRef = self.rParties.child("\(parti.id)").child("Songlist").childByAutoId()
        songRef.setValue(s)
    }
    
    func writeCurrentlyPlayingToParti(parti: Parti, playIndex: Int, isPlaying: Bool) {
        let dict = NSMutableDictionary()
        dict.setValue(playIndex, forKey: "playIndex")
        dict.setValue(isPlaying, forKey: "isPlaying")
        self.ref.child("Parties/\(parti.id)/CurrentlyPlaying").setValue(dict)
    }
    
    func writeStopPlayingToParti(parti: Parti) {
        let dict = NSMutableDictionary()
        dict.setValue(nil, forKey: "playIndex")
        dict.setValue(false, forKey: "isPlaying")
        self.ref.child("Parties/\(parti.id)/CurrentlyPlaying").setValue(dict)
    }
    
    func removeAllObservers() {
        self.ref.removeAllObservers()
    }
    
    func observeCurrentlyPlayingParti(parti: Parti, callback: @escaping (Int?, Bool) -> ()) {
        self.ref.child("Parties/\(parti.id)/CurrentlyPlaying").observe(DataEventType.value, with: {(snapshot) in
            if snapshot.exists() {
                if let val = snapshot.value as? [String:Any] {
                    if let index = val["playIndex"] as? Int, let isPlaying = val["isPlaying"] as? Bool{
                        callback(index, isPlaying)
                    } else {
                        callback(nil, false)
                    }
                }
            }
        })
    }
    
    func checkIfUserExists(user: SPTUser, callback: @escaping (Bool) -> ()) {
        self.ref.child("Users/\(user.uri.absoluteString)").observeSingleEvent(of: .value, with: { (snapshot) in
            
            callback(snapshot.exists())
        })
    }
    
    func writeNewUserSavedParti(user: SPTUser, id: String) {
        let ref = rUsers.child("\(user.uri)/SavedParties")
        ref.setValue(id)
    }
    
    func writeNewUser(user: SPTUser, completion: (() -> Void)!) {
        let newUserRef = self.ref.child("Users/").child(user.uri.absoluteString)
        let user = [
            "URI" : user.uri.absoluteString,
            "SavedParties" : []
        ] as [String : Any]
        newUserRef.setValue(user)
        completion()
    }
    
    func observeUserSavedParties(user: SPTUser, callback: @escaping ([Parti]) -> ()) {
        self.ref.child("Users/\(user.uri)/SavedParties").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                if let partiIds = snapshot.value as? [[String:String]] {
                    var parties: [Parti] = []
                    for id in partiIds {
                        if let idString = id["id"] {
                            self.getParti(partiId: idString, callback: { (parti) in
                                parties.append(parti)
                            })
                        }
                    }
                    callback(parties)
                }
            }
        })
    }
    
    func writeNewParti(name: String, settings: [String:String]){
        
        let createdParti = rParties.childByAutoId()
        let parti: [String: Any] = [
            "id": createdParti.key,
            "Name": name,
            "CurrentlyPlaying": [
                "isPlaying": false,
                "playIndex": -1
            ],
            "Settings": [
                "Privacy": settings["Privacy"],
                "Other": "Not set"
            ]
        ]
        
        if JSONSerialization.isValidJSONObject(parti){
            createdParti.setValue(parti)
        }else{
            print("Not valid format")
        }
    }
    
    func getParti(partiId: String, callback: @escaping (Parti) -> ()) {
        self.ref.child("Parties/\(partiId)").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                if let parti = snapshot.value as? [String:AnyObject] {
                    if let name = parti["Name"] as? String{
                        callback(Parti.init(name: name, id: partiId))
                    }
                }
            }
        })
    }
}
